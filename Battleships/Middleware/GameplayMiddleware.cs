using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using Battleships.Services;
using Microsoft.AspNetCore.Http;

namespace Battleships.Middleware
{
    public class GameplayMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly GameplayService _gameplayService;
        
        public GameplayMiddleware(RequestDelegate next, GameplayService gameplayService)
        {
            _next = next;
            _gameplayService = gameplayService;
        }

        public async Task Invoke(HttpContext context)
        {
            if (!context.WebSockets.IsWebSocketRequest)
            {
                await _next.Invoke(context);
                return;
            }

            CancellationToken ct = context.RequestAborted;
            WebSocket currentSocket = await context.WebSockets.AcceptWebSocketAsync();
           _gameplayService.AddPlayer(currentSocket);
           while (true)
           {
               if (ct.IsCancellationRequested)
               {
                   break;
               }

               var response = await _gameplayService.ReceiveShootCordsAsync(currentSocket, ct);
               if (string.IsNullOrEmpty(response))
               {
                   if (currentSocket.State != WebSocketState.Open)
                   {
                       break;
                   }
                   continue;
               }

               if (_gameplayService.CheckIfInGame(currentSocket))
               {
                   if (_gameplayService.GetWebSocketOpponentState(currentSocket) != WebSocketState.Open)
                   {
                       continue;
                   }

                   await _gameplayService.SendShootCordsAsync(currentSocket, response, ct);
               }
           }
        }
    }
}