using System;

namespace Battleships.Utilities
{
    public class GameboardTools
    {
        private static int[] _shipLengths = {2, 3, 3, 4, 5};
        private static Random _random = new Random();

        public static int[][] GenerateRandomGameboard()
        {
            int[][] gameboard = new int[10][];
            for (int i = 0; i < gameboard.Length; i++)
            {
                gameboard[i] = new int[10];
                for (int j = 0; j < gameboard[0].Length; j++)
                {
                    gameboard[i][j] = 0;
                }
            }

            foreach (int ship in _shipLengths)
            {
                bool added = false;
                while (!added)
                {
                    int x = (int) (gameboard.Length * _random.NextDouble());
                    int y = (int) (gameboard[0].Length * _random.NextDouble());
                    bool horizontal = ((int) (10 * _random.NextDouble())) % 2 == 0;
                    if (horizontal)
                    {
                        // Check for vertical space
                        bool hasSpace = true;
                        int leftDiff = x == 0 ? 0 : 1;
                        int rightDiff = x == 9 ? 0 : 1;
                        int topDiff = y == 0 ? 0 : 1;
                        int bottomDiff = y == 9 ? 0 : 1;
                        for (int i = 0; i < ship; i++)
                        {
                            if (y + i >= gameboard[0].Length)
                            {
                                hasSpace = false;
                                break;
                            }

                            if (gameboard[x][y + i] != 0)
                            {
                                hasSpace = false;
                                break;
                            }

                            if (gameboard[x - leftDiff][y + i] != 0 || gameboard[x + rightDiff][y + i] != 0)
                            {
                                hasSpace = false;
                                break;
                            }

                            bottomDiff = y + i == 9 ? 0 : 1;
                            if (i == ship - 1 && (gameboard[x][y + i + bottomDiff] != 0 ||
                                                  gameboard[x - leftDiff][y + i + bottomDiff] != 0 ||
                                                  gameboard[x + rightDiff][y + i + bottomDiff] != 0))
                            {
                                hasSpace = false;
                                break;
                            }

                            if (i == 0 && (gameboard[x][y + i - topDiff] != 0 ||
                                           gameboard[x - leftDiff][y + i - topDiff] != 0 ||
                                           gameboard[x + rightDiff][y + i - topDiff] != 0))
                            {
                                hasSpace = false;
                                break;
                            }
                        }

                        if (!hasSpace)
                        {
                            continue;
                        }

                        for (int i = 0; i < ship; i++)
                        {
                            gameboard[x][y + i] = 1;
                        }

                        added = true;
                    }
                    else
                    {
                        // Check for horizontal space
                        bool hasSpace = true;
                        int topDiff = y == 0 ? 0 : 1;
                        int bottomDiff = y == 9 ? 0 : 1;
                        int leftDiff = x == 0 ? 0 : 1;
                        int rightDiff = x == 9 ? 0 : 1;
                        for (int i = 0; i < ship; i++)
                        {
                            if (x + i >= gameboard.Length)
                            {
                                hasSpace = false;
                                break;
                            }

                            if (gameboard[x + i][y] != 0)
                            {
                                hasSpace = false;
                                break;
                            }

                            if (gameboard[x + i][y - topDiff] != 0 || gameboard[x + i][y + bottomDiff] != 0)
                            {
                                hasSpace = false;
                                break;
                            }

                            rightDiff = x + i == 9 ? 0 : 1;
                            if (i == ship - 1 && (gameboard[x + i + rightDiff][y] != 0 ||
                                                  gameboard[x + i + rightDiff][y - topDiff] != 0 ||
                                                  gameboard[x + i + rightDiff][y + bottomDiff] != 0))
                            {
                                hasSpace = false;
                                break;
                            }

                            if (i == 0 && (gameboard[x + i - leftDiff][y] != 0 ||
                                           gameboard[x + i - leftDiff][y - topDiff] != 0 ||
                                           gameboard[x + i - leftDiff][y + bottomDiff] != 0))
                            {
                                hasSpace = false;
                                break;
                            }
                        }

                        if (!hasSpace)
                        {
                            // No room there, check again
                            continue;
                        }

                        for (int i = 0; i < ship; i++)
                        {
                            gameboard[x + i][y] = 1;
                        }

                        added = true;
                    }
                }
            }

            return gameboard;
        }
    }
}