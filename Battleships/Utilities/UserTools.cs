using System;

namespace Battleships.Utilities
{
    public static class UserTools
    {
        private static Random _random = new Random();
        
        public static string GenerateUserName()
        {
            string firstPart = "Player";
            int secondPart = _random.Next(1000, 9999);
            string userName = firstPart + secondPart.ToString();
            return userName;
        }
    }
}