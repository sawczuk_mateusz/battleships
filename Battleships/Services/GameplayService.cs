using System;
using System.Collections.Concurrent;
using System.IO;
using System.Net.WebSockets;
using System.Security.AccessControl;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Battleships.Services
{
    public class GameplayService
    {
        private ConcurrentDictionary<Guid, WebSocket> _connectedPlayers = new ConcurrentDictionary<Guid, WebSocket>();

        private ConcurrentDictionary<WebSocket, WebSocket> _gamesInProgress = new ConcurrentDictionary<WebSocket, WebSocket>();

        private ConcurrentQueue<Guid> _playersQueue = new ConcurrentQueue<Guid>();

        public WebSocketState GetWebSocketOpponentState(WebSocket ws)
        {
            return _gamesInProgress[ws].State;
        }

        public bool CheckIfInGame(WebSocket ws)
        {
            return _gamesInProgress.ContainsKey(ws);
        }
        
        public void AddPlayer(WebSocket ws)
        {
            Guid socketId = Guid.NewGuid();
            _connectedPlayers.TryAdd(socketId, ws);
            _playersQueue.Enqueue(socketId);
            MatchPlayers();
        }
        
        private void MatchPlayers()
        {
            if (_playersQueue.Count > 1)
            {
                Guid player1;
                _playersQueue.TryDequeue(out player1);
                Guid player2;
                _playersQueue.TryDequeue(out player2);
                _gamesInProgress.TryAdd(_connectedPlayers[player1], _connectedPlayers[player2]);
                _gamesInProgress.TryAdd(_connectedPlayers[player2], _connectedPlayers[player1]);
                SendMatchConfirmation(_connectedPlayers[player1], "game");
                SendMatchConfirmation(_connectedPlayers[player2], "game");
            }
        }

        private Task SendMatchConfirmation(WebSocket socket, string data,
            CancellationToken ct = default(CancellationToken))
        {
            var buffer = Encoding.UTF8.GetBytes(data);
            var segment = new ArraySegment<byte>(buffer);
            return socket.SendAsync(segment, WebSocketMessageType.Text, true, ct);
        }

        public Task SendShootCordsAsync(WebSocket socket, string data,
            CancellationToken ct = default(CancellationToken))
        {
            var buffer = Encoding.UTF8.GetBytes(data);
            var segment = new ArraySegment<byte>(buffer);
            return _gamesInProgress[socket]
                .SendAsync(segment, WebSocketMessageType.Text, true, ct);
        }

        public async Task<string> ReceiveShootCordsAsync(WebSocket socket,
            CancellationToken ct = default(CancellationToken))
        {
            var buffer = new ArraySegment<byte>(new byte[8192]);
            using (var ms = new MemoryStream())
            {
                WebSocketReceiveResult result;
                do
                {
                    ct.ThrowIfCancellationRequested();
                    result = await socket.ReceiveAsync(buffer, ct);
                    ms.Write(buffer.Array, buffer.Offset, result.Count);
                } while (!result.EndOfMessage);

                ms.Seek(0, SeekOrigin.Begin);
                if (result.MessageType != WebSocketMessageType.Text)
                {
                    return null;
                }

                using (StreamReader reader = new StreamReader(ms, Encoding.UTF8))
                {
                    return await reader.ReadToEndAsync();
                }
            }
        }
    }
}