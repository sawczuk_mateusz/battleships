using Battleships.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace Battleships.Controllers
{
    [Route("api")]
    public class GameSettingsController : Controller
    {
        [HttpGet]
        [Route("gameboard")]
        public IActionResult GetGameboard()
        {
            return Ok(GameboardTools.GenerateRandomGameboard());
        }

        [HttpGet]
        [Route("name")]
        public IActionResult GetUserName()
        {
            return Ok(UserTools.GenerateUserName());
        }
    }
}